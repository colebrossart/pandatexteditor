package client;
import java.io.Serializable;

public class GoOffline implements Serializable{

		public static final long serialVersionUID = 1L;
		
		public boolean goOffline;
		
		public GoOffline(boolean goOffline) {
			this.goOffline = goOffline;
		}

		public boolean getGoOffline() {
			return goOffline;
		}
}
