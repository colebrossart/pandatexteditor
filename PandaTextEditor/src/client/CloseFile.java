package client;

import java.io.Serializable;

public class CloseFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String filepath;
	
	public CloseFile(String filepath) {
		this.filepath = filepath;
	}
	
	public String getFilePath() {
		return filepath;
	}

}
