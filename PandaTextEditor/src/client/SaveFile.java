package client;

import java.io.Serializable;

public class SaveFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String filename, text, username;

	private boolean replace, autosave;
	
	public SaveFile(String filename, String text, String username, boolean autosave) {
		this.filename = filename;
		this.text = text;
		this.username = username;
		this.replace = false;
		this.autosave = autosave;
	}

	public String getFilename() {
		return filename;
	}

	public String getText() {
		return text;
	}
	
	public String getUsername() {
		return username;
	}
	
	public boolean getReplace() {
		return replace;
	}
	
	public void setReplace(boolean b) {
		replace = b;
	}
	
	public boolean getAutoSave() {
		return autosave;
	}

}
