package client;
import java.io.Serializable;
import java.util.Vector;

public class MergeFileVector implements Serializable {

	private static final long serialVersionUID = 1L;
	private Vector<MergeFile> mergeFiles;
	
	public MergeFileVector(Vector<MergeFile> mergeFiles) {
		this.mergeFiles = mergeFiles;
	}
	
	public Vector<MergeFile> getMergeFileVector() {
		return mergeFiles;
	}
	
}
