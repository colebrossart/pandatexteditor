package client;

import java.io.Serializable;

public class LoginInfo implements Serializable {

	public static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private boolean success;
	
	public LoginInfo(String username, String password) {
		this.username = username;
		this.password = password;
		success = false;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean b) {
		success = b;
	}
}
