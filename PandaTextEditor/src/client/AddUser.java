package client;

import java.io.Serializable;

public class AddUser implements Serializable {

	private static final long serialVersionUID = 1L;
	private String filename;
	private String username;
	private String owner;
	
	public AddUser(String username, String owner, String filename) {
		this.username = username;
		this.owner = owner;
		this.filename = filename;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public String getFilename() {
		return filename;
	}

}