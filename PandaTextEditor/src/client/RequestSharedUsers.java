package client;

import java.io.Serializable;

public class RequestSharedUsers implements Serializable {

	private static final long serialVersionUID = 1L;
	private String filename;
	private String owner;
	private String[] usersWithAccess;
	
	public RequestSharedUsers(String filename, String owner) {
		this.filename = filename;
		this.owner = owner;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public String[] getUsersWithAccess() {
		return usersWithAccess;
	}
	
	public void setUsersWithAccesss(String[] usersWithAccess) {
		this.usersWithAccess = usersWithAccess;
	}
	
	public String getOwner() {
		return owner;
	}

}