package client;

import java.io.Serializable;

public class HasAccess implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String filename;
	private String owner;
	private boolean hasAccess;
	
	public HasAccess(String username, String filename, String owner) {
		this.username = username;
		this.filename = filename;
		this.owner = owner;
		this.hasAccess = true;
	}
	
	public String getOwner() {
		return owner;
	}

	public boolean hasAccess() {
		return hasAccess;
	}

	public void setHasAccess(boolean hasAccess) {
		this.hasAccess = hasAccess;
	}

	public String getUsername() {
		return username;
	}

	public String getFilename() {
		return filename;
	}

}
