package client;

import java.io.Serializable;

public class MergeFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String originalText;
	private String updatedText;
	private String filepath;
	private String client;
	
	public MergeFile(String originalText, String updatedText, String filepath, String client) {
		this.originalText = originalText;
		this.updatedText = updatedText;
		this.filepath = filepath;
		this.client = client;
	}
	
	public String getOriginalText() {
		return originalText;
	}
	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}
	public String getUpdatedText() {
		return updatedText;
	}
	public void setUpdatedText(String updatedText) {
		this.updatedText = updatedText;
	}
	public String getFilePath() {
		return filepath;
	}
	public void setFilePath(String filepath) {
		this.filepath = filepath;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	
}
