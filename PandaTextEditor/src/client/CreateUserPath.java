package client;

import java.io.Serializable;

public class CreateUserPath implements Serializable{

	private static final long serialVersionUID = 1L;
	private String username;
	
	public CreateUserPath(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	
}
