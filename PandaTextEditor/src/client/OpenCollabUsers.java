package client;

import java.io.Serializable;

public class OpenCollabUsers implements Serializable{

	private static final long serialVersionUID = 1L;
	private String username;
	private String[] collabUsers;
	
	public OpenCollabUsers(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String[] getCollabUsers() {
		return collabUsers;
	}
	
	public void setCollabUsers(String[] collabUsers) {
		this.collabUsers = collabUsers;
	}

}
