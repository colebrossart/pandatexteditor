package client;

import java.io.Serializable;

public class RemoveUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String filename;
	private String usernameToRemove;
	private String ownername;
	
	public RemoveUser(String filename, String username, String ownername) {
		this.filename = filename;
		this.usernameToRemove = username;
		this.ownername = ownername;
	}

	public String getFilename() {
		return filename;
	}

	public String getUsername() {
		return usernameToRemove;
	}
	
	public String getOwnername() {
		return ownername;
	}

}
