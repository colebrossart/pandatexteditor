package client;

import java.io.File;
import java.io.Serializable;

public class UpdatedFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String merge;
	private File file;
	private String client;
	
	public UpdatedFile(String merge, File file, String client) {
		this.merge = merge;
		this.file = file;
		this.client = client;
	}

	public String getMerge() {
		return merge;
	}

	public File getFile() {
		return file;
	}

	public String getClient() {
		return client;
	}
	

}
