package client;

import java.io.Serializable;

public class OpenSharedFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String owner;
	private String[] accessibleFiles;
	
	public OpenSharedFile(String username, String owner) {
		this.username = username;
		this.owner = owner;
	}

	public String[] getAccessibleFiles() {
		return accessibleFiles;
	}

	public void setAccessibleFiles(String[] accessibleFiles) {
		this.accessibleFiles = accessibleFiles;
	}

	public String getUsername() {
		return username;
	}

	public String getOwner() {
		return owner;
	}
	
}
