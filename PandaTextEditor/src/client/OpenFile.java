package client;

import java.io.Serializable;

public class OpenFile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String filename;
	private String username;
	private String text;
	
	public OpenFile(String filename, String text, String username) {
		this.filename = filename;
		this.username = username;
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

	public String getFilename() {
		return filename;
	}
	
	public String getUsername() {
		return username;
	}

	public void setText(String text) {
		this.text = text;
	}

}
