package client;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.JOptionPane;

import panda.JEditor;
import panda.OpenSharedFileDialog;
import panda.OpenSharedUsersGui;
import panda.PandaGui;
import panda.RemoveUserGui;


public class PandaClientListener extends Thread {

	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private String hostname;
	private int portNumber;
	private PandaGui pandaFrame;
	private Socket s;
	
	public PandaClientListener(PandaGui pandaFrame) {
		this.pandaFrame = pandaFrame;
		readClientConfig();
		s = null;
	}
	
	public boolean connect() {
		if(s != null) {
			return true;
		}
		try {
			s = new Socket(hostname, portNumber);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
			
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(pandaFrame, "Server Cannot be reached. Continue in offline mode.",
					"Log-in Failed", JOptionPane.ERROR_MESSAGE);
			pandaFrame.loginOffline();
			return false;
		} 
		return true;
	}
	
	
	public void sendMessage(Object o)    {
		try {
			oos.writeObject(o);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		try {
			while(true) {
				Object o = ois.readObject();
				if(o instanceof SigninInfo) {
					SigninInfo si = (SigninInfo) o;
					if(si.isSuccess()) {
						pandaFrame.loginSuccess(si.getUsername());
						CreateUserPath cup = new CreateUserPath(si.getUsername());
						oos.writeObject(cup);
						oos.flush();
					} else {
						JOptionPane.showMessageDialog(pandaFrame, "Username is taken",
								"Log-in Failed", JOptionPane.INFORMATION_MESSAGE);
					}
				} else if (o instanceof LoginInfo){
					LoginInfo li = (LoginInfo) o;
					if(li.isSuccess()) {
						pandaFrame.loginSuccess(li.getUsername());
					} else {
						JOptionPane.showMessageDialog(pandaFrame, "Username or password is invalid",
								"Log-in Failed", JOptionPane.INFORMATION_MESSAGE);
					}
				} else if (o instanceof OpenFile) {	
					OpenFile of = (OpenFile) o;
					pandaFrame.openFile(of.getFilename(), of.getText(), of.getUsername());
				} else if(o instanceof SaveFile) {
					SaveFile sf = (SaveFile) o;
					File f = new File("files/" + sf.getUsername() + "/"+ sf.getFilename());
					if(!sf.getReplace()) {
						JOptionPane.showMessageDialog(pandaFrame,
								"File Successfully Saved.",
								"Message",
								JOptionPane.INFORMATION_MESSAGE);
						
						pandaFrame.getSavedFiles().add(f);
						
					} else {
						int selection = JOptionPane.showConfirmDialog(pandaFrame, sf.getFilename() +
								" already exists. Do you want to replace it?", "Confirm Save as", JOptionPane.YES_NO_OPTION);
						if(selection == JOptionPane.YES_OPTION) {
							pandaFrame.save(pandaFrame.getSelectedTabIndex(), sf.getFilename(), sf.getText(), f);
							 
						}
					}
					pandaFrame.getTabbedPane().setTitleAt(pandaFrame.getSelectedTabIndex(), sf.getFilename());
					pandaFrame.getUsersAddButton().setEnabled(true);
					pandaFrame.getUsersRemoveButton().setEnabled(true);
					pandaFrame.getOpenFiles().get(pandaFrame.getSelectedTabIndex()).setFile(f);
					pandaFrame.getOpenFiles().get(pandaFrame.getSelectedTabIndex()).setFilename(f.getName());
					pandaFrame.getFileMap().put(f.getAbsolutePath(), pandaFrame.getOpenFiles().get(pandaFrame.getSelectedTabIndex()));
					pandaFrame.getOwnerMap().put(f.getAbsolutePath(), sf.getUsername());
					MergeFile mf = new MergeFile(sf.getText(),"", f.getAbsolutePath(), pandaFrame.getUsername());
					pandaFrame.getMergeFiles().addElement(mf);
					
					
				} else if (o instanceof GoOffline) {
					GoOffline go = (GoOffline) o;
					if(go.getGoOffline()) {
						pandaFrame.setOffline(true);
					}
				} else if(o instanceof AddUser) {
					JOptionPane.showMessageDialog(pandaFrame, "The user cannot be found!",
							"Error", JOptionPane.ERROR_MESSAGE);
				} else if(o instanceof RequestSharedUsers) {
					RequestSharedUsers r = (RequestSharedUsers)o;
					new RemoveUserGui(pandaFrame, r.getUsersWithAccess(), r.getFilename());
				} else if(o instanceof OpenCollabUsers) {
					OpenCollabUsers ocu = (OpenCollabUsers)o;
					new OpenSharedUsersGui(pandaFrame, ocu.getCollabUsers());
				} else if(o instanceof OpenSharedFile) {
					OpenSharedFile osf = (OpenSharedFile) o;
					new OpenSharedFileDialog(pandaFrame, osf.getAccessibleFiles(), osf.getOwner());
				} else if(o instanceof GetOpenFiles) {
					
					for(int i = 0; i < pandaFrame.getSavedFiles().size(); i++) {
						File f = pandaFrame.getSavedFiles().get(i);
						SaveFile sf = new SaveFile(f.getName(), 
								pandaFrame.getFileMap().get(f.getAbsolutePath()).getJTA().getText(), 
								pandaFrame.getOwnerMap().get(f.getAbsolutePath()), true);
						this.sendMessage(sf);	
					}
					if(pandaFrame.getOpenFiles().size() > 0) {
						
						File f2 = pandaFrame.getOpenFiles().get(pandaFrame.getSelectedTabIndex()).getFile();
						if(pandaFrame.getSavedFiles().contains(f2)) {
							HasAccess ha = new HasAccess(pandaFrame.getUsername(), f2.getName(), pandaFrame.getOwnerMap().get(f2.getAbsolutePath()));
							oos.writeObject(ha);
							oos.flush();
						}
					}
					
				} else if(o instanceof HasAccess) {
					//JOptionPane
					JOptionPane.showMessageDialog(pandaFrame, "You have been removed!",
							"", JOptionPane.INFORMATION_MESSAGE);
					pandaFrame.getCloseButton().doClick();
				} else if (o instanceof UpdateOpenFiles) {
					//loop through merge files adding updated text
					synchronized(this) {
						for(int i = 0; i < pandaFrame.getMergeFiles().size(); i++) {
							JEditor je = pandaFrame.getFileMap().get(pandaFrame.getMergeFiles().get(i).getFilePath());
							pandaFrame.getMergeFiles().get(i).setUpdatedText(je.getJTA().getText());
						}
						
						for(int i = 0; i < pandaFrame.getMergeFiles().size(); i++) {
							MergeFile mf =  pandaFrame.getMergeFiles().get(i);
							MergeFile mfs = new MergeFile(mf.getOriginalText(), mf.getUpdatedText(), mf.getFilePath(), mf.getClient());
							pandaFrame.getMergeFiles().remove(mf);
							pandaFrame.getMergeFiles().add(mfs);
							oos.writeObject(mfs);
							oos.flush();
						}				
					}
						
				} else if (o instanceof client.MergeFile) {
					synchronized(this) {
					
						MergeFile mf = (client.MergeFile) o;
						if(mf.getClient().equals(pandaFrame.getUsername())) {
							JEditor je = pandaFrame.getFileMap().get(mf.getFilePath());
							je.getJTA().setText(mf.getUpdatedText());
							for(int i = 0; i < pandaFrame.getMergeFiles().size(); i++) {
								if(pandaFrame.getMergeFiles().get(i).getFilePath().equals(mf.getFilePath())) {
									pandaFrame.getMergeFiles().get(i).setOriginalText(mf.getUpdatedText());
								}
							}
						}
					}
					
					
				}
			}
		} catch(ClassNotFoundException cnfe) {
			System.out.println("Panda Client run cnfe: " + cnfe.getMessage());
		} catch(ClassCastException cce) {
			System.out.println("Panda Client run cce: " + cce.getMessage());
		} catch (IOException ioe) {
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	private void readClientConfig() {
		
		Scanner scan = null;
		try {
			scan = new Scanner(new File("src/Client.config"));
			
			String s = scan.next();
			if(s.equals("ServerIP:")) {
				hostname = scan.next();
			}
			
			String s2 = scan.next();
			if(s2.equals("Port:")) {
				portNumber = scan.nextInt();
			}
			
			
		} catch (IOException ioe){
			System.out.println("ioe: " + ioe.getMessage());
		}
		scan.close();
	
	}

	public boolean isConneceted() {
		return !s.isClosed();
	}

}