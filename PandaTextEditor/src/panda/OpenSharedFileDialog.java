package panda;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import client.OpenFile;

public class OpenSharedFileDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JButton open;
	private JButton cancel;

	public OpenSharedFileDialog(PandaGui panda, String[] accessibleFiles, String owner) {
		this.setTitle("Choose a file:");
		this.setSize(200, 250);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		JTextField inputTextField = new JTextField();
		inputTextField.setEditable(false);
		
		JList<String> mlist = new JList<String>(accessibleFiles);
		JScrollPane jsp = new JScrollPane(mlist);
		jsp.setPreferredSize(new Dimension(100,150));
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		this.add(jsp, BorderLayout.CENTER);
				
		open = new JButton("Open...");
		cancel = new JButton("Cancel");
		JPanel jp = new JPanel();
		jp.setLayout(new GridLayout(1,2));
		jp.add(open);
		jp.add(cancel);
		this.add(jp, BorderLayout.SOUTH);
		
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(panda.getPandaClientListener().isConneceted()) {
					System.out.println("owner = " + owner);
					OpenFile of = new OpenFile(mlist.getSelectedValue(), "", owner);
					panda.getPandaClientListener().sendMessage(of);
					panda.getUsersAddButton().setEnabled(false);
					panda.getUsersRemoveButton().setEnabled(false);
					
					
					
				} else {
					JOptionPane.showMessageDialog(OpenSharedFileDialog.this,
							"File Failed to Open.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
				
				dispose();
			}
			
		});
		
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		});
		
		
		this.setVisible(true);
		
	}

}
