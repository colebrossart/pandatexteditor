package panda;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class PandaButton extends JButton {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String kind;
	private Image button;
	private Image hoverImg;
	private Image hoverImgServer;
	private boolean hover = false;
	
	public PandaButton(String name, String kind) 
	{
		this.name = name;
		this.kind = kind;
		this.setText(name);
		
		if(kind.equals("left")) {
			try {
				button = ImageIO.read(new File("img/ButtonLeft.png"));
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		else if(kind.equals("right")) {
			try {
				button = ImageIO.read(new File("img/ButtonRight.png"));
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		else if(kind.equals("center") || kind.equals("config")) {
			try {
				button = ImageIO.read(new File("img/ButtonCenter.png"));
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		else if(kind.equals("server")) {
			try{
				button = ImageIO.read(new File("img/ButtonServer2.png"));
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			hoverImg = ImageIO.read(new File("img/hover2.jpg"));
			hoverImgServer = ImageIO.read(new File("img/hover2Server.png"));
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		addMouseListener(new java.awt.event.MouseAdapter() {  
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                hover = true;
                repaint();
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hover = false;
                repaint();
            }
        });
		
		this.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				repaint();
				
			}
			
		});
		
		
	}

	protected void paintComponent(Graphics g) 
	{
		Graphics2D g2 = (Graphics2D) g.create();
		if(!hover) {
			g2.drawImage(button , 0, 0, this.getWidth(), this.getHeight(), null);
		}
		else {
			if(kind.equals("server")) {
				g2.drawImage(hoverImgServer , 0, 0, this.getWidth(), this.getHeight(), null);
			}
			else {
				g2.drawImage(hoverImg , 0, 0, this.getWidth(), this.getHeight(), null);
			}
			
		}
	    this.setForeground(Color.white);
	    
	    if(kind.equals("left")) {
	    	g2.drawString(getText(), this.getWidth()/2 - this.getWidth()/8, this.getHeight()/2);
	    } 
	    else if(kind.equals("right")) {
	    	g2.drawString(getText(), this.getWidth()/12, this.getHeight()/2);
	    }
	    else if(kind.equals("center")) {
	    	g2.drawString(getText(), this.getWidth()/2 - this.getWidth()/8, this.getHeight()/2);
	    }
	    else if(kind.equals("config")) {
	    	g2.drawString(getText(), this.getWidth()/8, this.getHeight()/2);
	    }
	    else if(kind.equals("server")) {
	    	g2.drawString(getText(), this.getWidth()/2 - this.getWidth()/24, this.getHeight()/2);
	    }
	    
	    
	    g2.dispose();
	     
	}
	
	public String getText()
	{
		return name;
	}
	
	public void setText(String s) {
		this.name = s;
	}
}
