package panda;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import client.CloseFile;
import client.MergeFile;
import client.OpenCollabUsers;
import client.PandaClientListener;
import client.RequestSharedUsers;

public class PandaGui extends JFrame {
	
	public static final long serialVersionUID = 1;
	
	//menu variables
	private JTabbedPane tabbedpane;	
	private JMenuBar jmb;
	private JMenu file;
	private JMenu edit;
	private JMenu spellcheck;
	private JMenu users;
	private JMenuItem f_new;
	private JMenuItem f_open;
	private JMenuItem f_save;
	private JMenuItem f_close;
	private JMenuItem e_cut;
	private JMenuItem e_copy;
	private JMenuItem e_paste;
	private JMenuItem e_selectAll;	
	private JMenuItem sc_run;
	private JMenuItem sc_configure;
	private JMenuItem u_add;
	private JMenuItem u_remove;
	
	private JFileChooser jfc;
	private ArrayList<JEditor> openFiles;	//All JEditors indexed by tab
	private Vector<File> savedFiles;		//All saved files
	private ArrayList<String> dataFiles;	//command line keyboard and wordlist files
	private HashMap<String, JEditor> fileMap;
	private HashMap<String, String> ownerMap;
	
	private int numTabs;					//total number of tabs
	private int previousTabIndex;			//index of previous tab
	
	private boolean newTab = false;
	private boolean isOuterPanelonDisplay = false;
	private boolean first = true;
	private boolean justClosed = false;
	
	private JPanel cards;
	private ImageIcon logo;
	private JLabel pandaLabel;
	private ClientPanel clientPanel;
	private LoginPanel loginPanel;
	private SignupPanel signupPanel;
	private JPanel textEditorPanel;
	
	private String username;
	private boolean online; 
	private boolean saveOffline = false;
	private boolean openOffline = false;
	
	private PandaClientListener pandaClientListener;
	private int selectedTabIndex;
	
	private Vector<MergeFile> mergeFiles;
	
	public PandaGui(String[] args) {
		super("Panda");	
		
	    UIManager.put("TabbedPane.background", new Color(204, 255, 229));
		UIManager.put("TabbedPane.selected", new Color(115, 230, 142));
		UIManager.put("TabbedPane.foreground", new Color(0, 51, 25));
		UIManager.put("TabbedPane.borderHightlightColor", new Color(0, 51, 25));
		UIManager.put("TabbedPane.contentAreaColor", new Color(115, 230, 142));

		setUIFont (new javax.swing.plaf.FontUIResource("Papyrus",Font.PLAIN,12));
		instantiateComponents();
		createGUI();
		addActions();
		addGraphics();
		dataFiles = new ArrayList<String>();	
		for(int i = 0; i < args.length; i++) {
			dataFiles.add(args[i]);
		}
		
		
	}
	
	//http://stackoverflow.com/questions/8707082/set-a-default-font-for-whole-ios-app
	public static void setUIFont (javax.swing.plaf.FontUIResource f) {
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements()) {
	    	Object key = keys.nextElement();
	    	Object value = UIManager.get (key);
	    	if (value != null && value instanceof javax.swing.plaf.FontUIResource)
	    		UIManager.put (key, f);
	      	}
    } 
	
	public void instantiateComponents() {
		
		createMenu();
		createMenuItems();
		addShortcuts();
		addFileMenuItems();
		createFileChooser();
				
		numTabs = 0;
		tabbedpane = new JTabbedPane();
		openFiles = new ArrayList<JEditor>();
		savedFiles = new Vector<File>();
		logo = new ImageIcon("img/logo.jpg");
		pandaLabel = new JLabel(logo);
		
		clientPanel = new ClientPanel();
		pandaClientListener = new PandaClientListener(this);
		loginPanel = new LoginPanel(pandaClientListener);
		signupPanel = new SignupPanel(pandaClientListener);
		textEditorPanel = new JPanel();
		textEditorPanel.setLayout(new BorderLayout());
		fileMap = new HashMap<String, JEditor>();
		ownerMap = new HashMap<String, String>();
		mergeFiles = new Vector<MergeFile>();
		

	}
	
	public void createGUI() {
		setSize(600, 600);		
		setLocation(650, 70);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cards = new JPanel();
		cards.setLayout(new CardLayout());
		cards.add("client", clientPanel);
		cards.add("signup", signupPanel);
		cards.add("login", loginPanel);
		cards.add("textEditor", textEditorPanel);
		this.add(cards);
	}
	
	//add actions to all buttons in menu
	public void addActions() {
		//creates new tab
		f_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JEditor je = new JEditor("");	
				tabbedpane.add("new", je.getJSP());
				tabbedpane.setFont(new Font("Papyrus", Font.PLAIN, 12));
				
				openFiles.add(numTabs, je);
				
				u_add.setEnabled(false);
				u_remove.setEnabled(false);
				File f = new File("");
				je.setFile(f);
				fileMap.put(f.getAbsolutePath(), je);
				ownerMap.put(f.getAbsolutePath(), "");
				je.setFilename(f.getName());
				tabbedpane.setSelectedIndex(numTabs);
				numTabs++;	
				
				if(numTabs == 1) {
					addEditandSpellCheckToMenu();
					pandaLabel.setVisible(false);
					tabbedpane.setVisible(true);
	            	f_close.setEnabled(true);
					textEditorPanel.add(tabbedpane, BorderLayout.CENTER);	
				}
			}
		});
		
		//opens new tab from file chooser
		f_open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				if(online) {
			    	Object[] options = {"Online", "Offline"};
			    	int selection = JOptionPane.showOptionDialog(PandaGui.this, 
			    			"Where would you like to open the file?", 
			    			"Open...", JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, 
			    			null, options, options[0]);
			    	if(selection == 0) {
			    		OpenCollabUsers ocu = new OpenCollabUsers(PandaGui.this.getUsername());
			    		pandaClientListener.sendMessage(ocu);
			    		
			    	}
			    	else if(selection == 1) {
			    		openOffline = true;
			    	}
			    	
			    }
			    if(!online || openOffline) {
			    	openOffline = false;
				
					int returnVal = jfc.showOpenDialog(null);
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						File f = jfc.getSelectedFile();	
						if(!openFiles.contains(f)) {
							String text = readFile(f);	
							JEditor je = new JEditor(text);	
							fileMap.put(f.getAbsolutePath(), je);
							ownerMap.put(f.getAbsolutePath(), getUsername());
							tabbedpane.add(f.getName(), je.getJSP());
							openFiles.add(numTabs, je);
							savedFiles.add(f);
							je.setFile(f);
							je.setFilename(f.getName());
							tabbedpane.setSelectedIndex(numTabs);
							int i = tabbedpane.getSelectedIndex();
							openFiles.get(i).setFile(f);
							openFiles.get(i).setFilename(f.getName());
							u_add.setEnabled(false);
							u_remove.setEnabled(false);
							numTabs++;
							if(numTabs == 1) {
								addEditandSpellCheckToMenu();
								pandaLabel.setVisible(false);
								tabbedpane.setVisible(true);
								textEditorPanel.add(tabbedpane, BorderLayout.CENTER);
				            	f_close.setEnabled(true);
	
							}
						}
						else {
							tabbedpane.setSelectedIndex(openFiles.indexOf(f));
						}
					}
			    }
			}
		});
	
		//Saves a file to given location
		f_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(numTabs > 0) {	
					selectedTabIndex = tabbedpane.getSelectedIndex();			
					JEditor je = openFiles.get(selectedTabIndex);
					je.setRecentlySaved(true);
					String text = je.getJTA().getText();
				    if(online) {
				    	Object[] options = {"Online", "Offline"};
				    	int selection = JOptionPane.showOptionDialog(PandaGui.this, 
				    			"Where would you like to save the file?", 
				    			"Save...", JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, 
				    			null, options, options[0]);
				    	if(selection == 0) {
				    		new SaveFileDialog(username, text, PandaGui.this);
				    		
				    	}
				    	else if(selection == 1) {
				    		saveOffline = true;
				    	}
				    	
				    }
				    if(!online || saveOffline){
				    	saveOffline = false;
						int retrival = jfc.showSaveDialog(null);
					    if (retrival == JFileChooser.APPROVE_OPTION) {
					    	File f = jfc.getSelectedFile();
					    	fileMap.put(f.getAbsolutePath(), je);
					    	ownerMap.put(f.getAbsolutePath(), getUsername());
					    	String filename = txtExtension(f);		//makes sure filename ends in .txt	    			
				        	if(!savedFiles.contains(f)) {
				        		save(selectedTabIndex, filename, text, f);
				        		savedFiles.add(f);
				        	}
				        	else {
					    		int selection = JOptionPane.showConfirmDialog(PandaGui.this, jfc.getSelectedFile().getName() +
										" already exists. Do you want to replace it?", "Confirm Save as", JOptionPane.YES_NO_OPTION);
								if(selection == JOptionPane.YES_OPTION) {
									save(selectedTabIndex, filename, text, f);
									 
								}
				        	}
				        	je.setFile(f);
				        	je.setFilename(f.getName());
				        	u_add.setEnabled(false);
							u_remove.setEnabled(false);
						}	
				    }
								
				}
			}
		});
		
		//Closes the current tab
		f_close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				int count = 0;
				for(int j = 0; j < openFiles.size(); j++) {
					if(openFiles.get(j).getFile() == je.getFile()) {
						count++;
					}
				}
				if(count == 1) {
					savedFiles.remove(openFiles.get(i).getFile());
					fileMap.remove(je.getFile().getAbsolutePath());
					ownerMap.remove(je.getFile());
				}
				openFiles.remove(je);
				justClosed = true;
				tabbedpane.remove(tabbedpane.getSelectedComponent());
				
				numTabs--;
				if(numTabs == 0) {
					removeEditAndSpellCheckFromMenu();
					tabbedpane.setVisible(false);
					f_close.setEnabled(false);
					pandaLabel = new JLabel(new ImageIcon("img/logo.jpg"));
					textEditorPanel.add(pandaLabel);
				}
				else if(!isSavedFile()) {
					u_add.setEnabled(false);
					u_remove.setEnabled(false);
				}
				CloseFile cf = new CloseFile(je.getFile().getAbsolutePath());
				pandaClientListener.sendMessage(cf);
			}
		});
		
		
		//cut selected text
		e_cut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				je.getJTA().cut();
			}
		});
		//copy selected text
		e_copy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				je.getJTA().copy();
			}
		});
		
		//paste from clip board
		e_paste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				je.getJTA().paste();
			}
		});
		
		//select all text in tab's JTextArea 
		e_selectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				je.getJTA().selectAll();
			}
		});
		
		//Run Spell Check!!!
		sc_run.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
				
				JPanel outerPanel;
				
				//Takes care of cases where window is open and must first be terminated
				if(!isOuterPanelonDisplay || je.closedOuterPanel() || newTab || je.getRecentlySaved()) {
					if(je.selectedWordListFile()) {
						dataFiles.add(0, je.getSelectedWordListFile().getAbsolutePath());
						je.closeOuterPanel();
					}
					if(je.selectedKeyboardFile()) {
						dataFiles.add(0, je.getSelectedKeyBoardFile().getAbsolutePath());
						je.closeOuterPanel();
					}
					je.createOuterPanel(dataFiles);
					
					outerPanel = je.getOuterPanel();
					je.checkForErrors();
					je.setIsSpellCheckerOpen(true);
					textEditorPanel.add(outerPanel, BorderLayout.EAST);
					isOuterPanelonDisplay = true;
					newTab = false;
				}
				else {
					outerPanel = je.getOuterPanel();
					je.checkForErrors();
				}
				
				
				if(je.getNumErrors() > 0) {
					je.focusSpellingError(0);
					
					CardLayout c = (CardLayout) outerPanel.getLayout();
					c.show(outerPanel, "SpellCheck");		//switch to SpellCheckLayout
				}
	
				revalidate();
				repaint();
				
			}
		});
		
		//Opens configure menu to change wordlist and keyboard files
		sc_configure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				int i = tabbedpane.getSelectedIndex();
				JEditor je = openFiles.get(i);
			
				JPanel outerPanel;

				if(!isOuterPanelonDisplay || je.closedOuterPanel() || newTab || je.getRecentlySaved()) {
					je.createOuterPanel(dataFiles);
					outerPanel = je.getOuterPanel();
					je.setIsConfigureOpen(true);
					textEditorPanel.add(outerPanel, BorderLayout.EAST);
					isOuterPanelonDisplay = true;
					newTab = false;
				}
				else {
					outerPanel = je.getOuterPanel();
				}
				
				CardLayout c = (CardLayout) outerPanel.getLayout();
				c.show(outerPanel, "Configure");
				
				revalidate();
				repaint();
				
			}
		});
		
		u_add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new AddUserGui(PandaGui.this);
			}
		
		});
		
		u_remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String filename = getFilename();
				RequestSharedUsers ru = new RequestSharedUsers(filename, username);
				pandaClientListener.sendMessage(ru);
			}
			
		});
		
		//Listens for when the user changes the tab
		tabbedpane.addChangeListener(new ChangeListener() {
	        public void stateChanged(ChangeEvent e) {
	            if(first) {
	            	previousTabIndex = 0;
	            	first = false;
	            }
	            else if(!justClosed && previousTabIndex > 0){
		            JEditor je = openFiles.get(previousTabIndex);
		            if(je.isSpellCheckerOpen()) {
						je.closeOuterPanel();
					}
		            if(je.isConfigureOpen()) {
						je.closeOuterPanel();
					}

		            JEditor je2 = openFiles.get(getSelectedTabIndex());
		            if(!ownerMap.get(je2.getFile()).equals(getUsername())) {
		            	u_add.setEnabled(false);
						u_remove.setEnabled(false);
		            } else {
		            	u_add.setEnabled(true);
						u_remove.setEnabled(true);
		            }
	            }
	            
	            
	            
	            justClosed = false;           
	            previousTabIndex = tabbedpane.getSelectedIndex();
	            newTab = true;
	        }
	    });
		
		clientPanel.getLoginButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout)(cards.getLayout());
				cl.show(cards, "login");
			}
		});
		clientPanel.getSignupButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout)(cards.getLayout());
				cl.show(cards, "signup");
			}
		});
		clientPanel.getOfflineButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout)(cards.getLayout());
				cl.show(cards, "textEditor");
				jmb.add(file);
			}
		});
		
	}
	
	//create JMenu components
	public void createMenu() {
		jmb = new JMenuBar() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g)
			{
				g.drawImage(Toolkit.getDefaultToolkit().getImage("img/menu5.jpg"),0,0,this);
			}
		 };
		file = new JMenu("File");
		edit = new JMenu("Edit");
		spellcheck = new JMenu("SpellCheck");
		users = new JMenu("Users");
		
	}
	
	//Create Menu Item componenets
	public void createMenuItems() {
		f_new = new JMenuItem("New");
		f_new.setIcon(new ImageIcon("img/menu_items/new.png"));
		f_open = new JMenuItem("Open");
		f_open.setIcon(new ImageIcon("img/menu_items/open.png"));
		f_save = new JMenuItem("Save");
		f_save.setIcon(new ImageIcon("img/menu_items/save.png"));
		f_close = new JMenuItem("Close");
		f_close.setEnabled(false);
		f_close.setIcon(new ImageIcon("img/menu_items/close.png"));
		e_cut = new JMenuItem("Cut");
		e_cut.setIcon(new ImageIcon("img/menu_items/cut.png"));
		e_copy = new JMenuItem("Copy");
		e_copy.setIcon(new ImageIcon("img/menu_items/copy.png"));
		e_paste = new JMenuItem("Paste");
		e_paste.setIcon(new ImageIcon("img/menu_items/paste.png"));
		e_selectAll = new JMenuItem("Select All");
		e_selectAll.setIcon(new ImageIcon("img/menu_items/select.png"));
		sc_run = new JMenuItem("Run");
		sc_run.setIcon(new ImageIcon("img/menu_items/run.png"));
		sc_configure = new JMenuItem("Configure");
		sc_configure.setIcon(new ImageIcon("img/menu_items/configure.png"));
		u_add = new JMenuItem("Add");
		u_add.setEnabled(false);
		u_remove = new JMenuItem("Remove");
		u_remove.setEnabled(false);
		
		
	}
	
	//Add keyboard shortcuts
	public void addShortcuts() {
		
		f_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		f_open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		f_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		e_cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		e_copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		e_paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		e_selectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		sc_run.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		
	}
	
	//Only add file to menu at first 
	public void addFileMenuItems() {
		file.add(f_new);
		file.add(f_open);
		file.add(f_save);
		file.add(f_close);
		
		edit.add(e_cut);
		edit.add(e_copy);
		edit.add(e_paste);
		edit.add(e_selectAll);
		
		spellcheck.add(sc_run);
		spellcheck.add(sc_configure);
		
		users.add(u_add);
		users.add(u_remove);
			
		setJMenuBar(jmb);
	}
	
	
	//When a tab is opened add edit and spellcheck options
	public void addEditandSpellCheckToMenu() {
		
		jmb.add(edit);
		jmb.add(spellcheck);
		jmb.add(users);
		setJMenuBar(jmb);
	}
	
	//When all tabs are closed then remove edit and spellcheck preferences
	public void removeEditAndSpellCheckFromMenu() {
		jmb.remove(edit);
		jmb.remove(spellcheck);
		jmb.remove(users);
	}
	
	public void createFileChooser() {
		jfc = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("txt files (*.txt)", "txt");
		jfc.setFileFilter(filter);
	}
	
	//make sure text file ends in .txt
	public String txtExtension(File f) {
		String filename = f.getName();
    	if(filename.length() < 4 || !filename.substring(filename.length() - 4).equals(".txt")) {
    		filename += ".txt";
    	}
    	return filename;
	}
	
	//write to file
	public void save(int i, String filename, String text, File f) {
		try (FileWriter fw = new FileWriter(f)) {
			tabbedpane.setTitleAt(i, filename);
            fw.write(text.toString());
            fw.close();
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }   
	}
	
	
	//read a given file and return text
	public String readFile(File f) {
		String text = "";			
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(f.getAbsolutePath());
			br = new BufferedReader(fr);
			String line = br.readLine();
			text = line;
			
			while (line != null) {
				line = br.readLine();
				if(line != null) {
					text += "\n" + line;
				}
			}
		}
		catch (FileNotFoundException fnfe) {
			System.out.println("FileNotFoundException: " + fnfe.getMessage());
		}
		catch (IOException ioe) {
			System.out.println("IOException: " + ioe.getMessage());
		}
		finally {
			try {
				if (br != null) {
					br.close();
				}
				if (fr != null) {
					fr.close();
				}
			}
		
			catch (IOException ioe) {
				System.out.println("IOException closing file: " + ioe.getMessage());
			}
		}
		return text;
	}
	
	public void addGraphics() {
		
		textEditorPanel.setBackground(Color.WHITE); 
		
		ImageIcon desktop_icon = new ImageIcon("img/desktop_icon.png");
		ImageIcon mouse = new ImageIcon("img/mouse2.png");
		
		textEditorPanel.add(pandaLabel, BorderLayout.CENTER);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Cursor c = toolkit.createCustomCursor(mouse.getImage(), new Point(0, 0), "custom cursor");
		this.setCursor (c);
			
			
		Class<?> applicationClass = null;
		try {
			applicationClass = Class.forName("com.apple.eawt.Application");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Method getApplicationMethod = null;
		try {
			getApplicationMethod = applicationClass.getMethod("getApplication");
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		Method setDockIconMethod = null;
		try {
			setDockIconMethod = applicationClass.getMethod("setDockIconImage", java.awt.Image.class);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		Object macOSXApplication = null;
		try {
			macOSXApplication = getApplicationMethod.invoke(null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		try {
			setDockIconMethod.invoke(macOSXApplication, desktop_icon.getImage());
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public void loginOffline() {
		CardLayout cl = (CardLayout)(cards.getLayout());
		cl.show(cards, "textEditor");
		jmb.add(file);
	}
	
	public void loginSuccess(String username) {
		CardLayout cl = (CardLayout)(cards.getLayout());
		cl.show(cards, "textEditor");
		jmb.add(file);
		online = true;
		this.username = username;
	}
	
	public void setOffline(boolean b) {
		online = !b;
	}

	public void openFile(String filename, String text, String owner) {
		File f = new File("files/" + owner + "/"+ filename);	
		if(!fileMap.containsKey(f)) {
			JEditor je = new JEditor(text);
			fileMap.put(f.getAbsolutePath(), je);
			ownerMap.put(f.getAbsolutePath(), owner);
			tabbedpane.add(f.getName(), je.getJSP());
			openFiles.add(numTabs, je);
			savedFiles.add(f);
			openFiles.get(numTabs).setFile(f);
			openFiles.get(numTabs).setFilename(f.getName());
			
			MergeFile mf = new MergeFile(text, "", f.getAbsolutePath(), getUsername());
			mergeFiles.addElement(mf);
			
			tabbedpane.setSelectedIndex(numTabs);
			
			numTabs++;
			if(numTabs == 1) {
				addEditandSpellCheckToMenu();
				pandaLabel.setVisible(false);
				tabbedpane.setVisible(true);
				textEditorPanel.add(tabbedpane, BorderLayout.CENTER);
            	f_close.setEnabled(true);
			}
		}
		else {
			JEditor je = fileMap.get(f);
			tabbedpane.setSelectedIndex(openFiles.indexOf(je));		
		}
		
	}
		 
	 public PandaClientListener getPandaClientListener() {
		 return pandaClientListener;
	 }
	 
	 public Vector<File> getSavedFiles() {
		 return savedFiles;
	 }
	 
	 public int getSelectedTabIndex() {
		 return tabbedpane.getSelectedIndex();
	 }
	 
	 public JTabbedPane getTabbedPane() {
		 return tabbedpane;
	 }
	 
	 public JMenuItem getUsersAddButton() {
		 return u_add;
	 }
	 
	 public JMenuItem getUsersRemoveButton() {
		 return u_remove;
	 }
	 
	 public String getUsername() {
		 return username;
	 }
	 
	 public ArrayList<JEditor> getOpenFiles() {
		 return openFiles;
	 }
	 
	 public String getFilename() {		 
		 return openFiles.get(tabbedpane.getSelectedIndex()).getFilename();
	 }
	 
	 public boolean isSavedFile() {
		 for(File f: savedFiles) {
			 if(f.getName().equals(getFilename())) {
				 return true;
			 }
		 }
		 return false;
	 }

	public HashMap<String, JEditor> getFileMap() {
		return fileMap;
	}

	public HashMap<String, String> getOwnerMap() {
		return ownerMap;
	}
	
	public JMenuItem getCloseButton() {
		return f_close;
	}
	
	public JFileChooser getJFileChooser() {
		return jfc;
	}

	public Vector<MergeFile> getMergeFiles() {
		return mergeFiles;
	}
}

