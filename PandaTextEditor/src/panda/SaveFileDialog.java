package panda;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import client.SaveFile;

public class SaveFileDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private String mSelection;
	private PandaButton mCancelButton;
	private PandaButton mSelectionButton;
	private String username;
	public SaveFileDialog(String username, String text, PandaGui panda) {
		this.setTitle("Files");
		this.username = username;
		this.setSize(150, 200);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		mSelection = "";
		this.setLocation(800,300);
		
		
		mSelectionButton = new PandaButton("Save", "left");
		mCancelButton = new PandaButton("Cancel", "right");
		JTextField inputTextField = new JTextField();
		String[] data = getFiles();
		JList<String> mlist = new JList<String>(data);
		JScrollPane jsp = new JScrollPane(mlist);
		jsp.setPreferredSize(new Dimension(100,150));
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		mSelectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mSelection = inputTextField.getText();
				
				if(panda.getPandaClientListener().isConneceted()) {
					SaveFile sf = new SaveFile(mSelection, text, username, false);
					panda.getPandaClientListener().sendMessage(sf);
					
				} else {
					JOptionPane.showMessageDialog(SaveFileDialog.this,
							"File Failed to Save.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
				
				dispose();
			}
		});
	
		mCancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mSelection = "";
				dispose();
			}
		});
		
		mlist.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				String file = mlist.getSelectedValue();
				inputTextField.setText(file);
			}
			
		});
		
		
		Box contentBox = Box.createVerticalBox();
		contentBox.add(jsp);
		contentBox.add(inputTextField);
		
		Box buttonBox = Box.createHorizontalBox();
		
		buttonBox.add(mSelectionButton);
		buttonBox.add(mCancelButton);
		contentBox.add(buttonBox);
		add(contentBox);
		
		pack();
		
		setModal(true);
		setVisible(true);
	}
	
	public String select() {
		
		return mSelection;
	}
	
	public String[] getFiles() {
		ArrayList<String> files = new ArrayList<String>();
		try(BufferedReader br = new BufferedReader(new FileReader("users/" + username + ".txt"))) {
			String line = br.readLine();
			while(line != null) {
				files.add(line);
				line = br.readLine();
			}
			br.close();
			
			
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} 
		
		String[] data = new String[files.size()];
		for(int i = 0; i < files.size(); i++) {
			data[i] = files.get(i);
		}
		return data;
	}
	
}
