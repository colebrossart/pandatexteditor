package panda;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class MyArrowButton extends JButton {
	
	private static final long serialVersionUID = 1L;
	private Image arrow;
	
	public MyArrowButton() {
		try {
			arrow = ImageIO.read(new File("img/arrow4.png"));
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.drawImage(arrow , 0, 0, this.getWidth(), this.getHeight(), null);
	}
}
