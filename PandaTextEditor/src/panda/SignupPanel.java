package panda;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import client.PandaClientListener;
import client.SigninInfo;


public class SignupPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private ImageIcon logo;
	private JLabel pandaLabel;
	
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JLabel repeatLabel;
	
	private JTextField usernameText;
	private JPasswordField passwordText;
	private JPasswordField repeatText;
	
	private String username;
	private String passwordString;
	private char[] password;
	private char[] repeat;
	
	private PandaButton login;
	private boolean passwordsMatch;
	private boolean passwordValid;
	
	private PandaClientListener pandaClientListener;
	
	public SignupPanel(PandaClientListener pcl) {
		this.pandaClientListener = pcl;
		instantiateComponents();
		createGUI();
		addActions();
	}

	private void instantiateComponents() {
		logo = new ImageIcon("img/logo2.jpg");
		pandaLabel = new JLabel(logo);
		
		usernameLabel = new JLabel("Username: ");
		passwordLabel = new JLabel("Password: ");
		repeatLabel = new JLabel("Repeat: ");
		
		usernameText = new JTextField();
		passwordText = new JPasswordField();
		repeatText = new JPasswordField();
		
		login = new PandaButton("Login", "center");
		username = "";
		passwordString = "";
		
	}
	
	private void createGUI() {
		this.setBackground(Color.white);
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0, 0, 20, 0);
		this.add(pandaLabel, gbc);
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		this.add(usernameLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 120;
		this.add(usernameText, gbc);
		gbc.ipadx = 0;
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(passwordLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 120;
		this.add(passwordText, gbc);
		gbc.ipadx = 0;
		gbc.gridx = 0;
		gbc.gridy = 3;
		this.add(repeatLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 120;
		this.add(repeatText, gbc);
		gbc.gridy = 4;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.ipadx = 50;
		gbc.insets = new Insets(20, 0, 0, 0);
		this.add(login, gbc);
	}
	
	private void addActions() {
		login.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				username = usernameText.getText();
				password = passwordText.getPassword();
				repeat = repeatText.getPassword();		
				
				if(!isValid(password, repeat)) {
					
					if(!passwordsMatch) {
						JOptionPane.showMessageDialog(SignupPanel.this, "Password and repeat do not match",
								"Sign-up Failed", JOptionPane.ERROR_MESSAGE);
					}
					
					if(!passwordValid)
					JOptionPane.showMessageDialog(SignupPanel.this, 
							"Password contain at least:\n1 - number and 1 - uppercase letter",
							"Sign-up Failed", JOptionPane.ERROR_MESSAGE);
				} else {
					if(pandaClientListener.connect()) {
						SigninInfo si = new SigninInfo(username, passwordString);
						pandaClientListener.sendMessage(si);
					}
					
					passwordString = "";
				}	
			}
		});
	}
	
	public boolean isValid(char[] password, char[] repeat) {
		boolean number = false;
		boolean uppercase = false;
		passwordsMatch = true;
		passwordValid = false;
		
		if(password.length != repeat.length) {
			passwordsMatch = false;
			passwordValid = false;
		} else {
			for(int i = 0; i < password.length; i++) {
				
				
				
				
				if(password[i] != repeat[i]) {
					passwordsMatch = false;
				}
				if(password[i] >= '0' && password[i] <= '9') {
					number = true;
					char c = password[i] += 5;
					if(c > '9') {
						c -= 9;
					}
					passwordString += c;
				}
				
				else if(password[i] >= 'A' && password[i] <= 'Z') {
					uppercase = true;
					char c = password[i] += 13;
					if(c > 'Z') {
						c -= 26;
					}
					passwordString += c;
				}
				else {
					char c = password[i] += 13;
					if(c > 'z') {
						c -= 26;
					}
					passwordString += c;
				}
			}
		}
		
		if(uppercase && number) {
			passwordValid = true;
		}
		
		if(passwordValid && passwordsMatch) {
			return true;
		}
		
		return false;
	}
	
	
}
