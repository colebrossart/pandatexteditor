package panda;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.MatchResult;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import autocorrect.Autocorrect;

public class JEditor {
		
	private JTextArea jta;
	private JScrollPane jsp;
	
	private File f;
	private File kbfile;
	private File wlfile;
	private Document d;
	
	private Autocorrect spellchecker;
	
	private ArrayList<ArrayList<String> > optionSet;	//Double nested Array list of each errors suggestions
	private ArrayList<String> errors;					//Array list of errors
	private ArrayList<Integer> startIndices;			//Array of starting indices of errors
	private ArrayList<Integer> endIndices;				//Array of ending indices of errors
	
	private int wordIndex;
	private JFileChooser jfc_kbfiles;
	private JFileChooser jfc_wlfiles;
	
	private JPanel outerPanel;
	private JPanel spellCheckWindow;
	private JPanel configure;
	private JPanel grid;
	private JPanel row1;
	private JPanel row2;
	private JPanel box3;
	
	private PandaButton ignore;
	private PandaButton add;
	private PandaButton change;
	private PandaButton close;
	private PandaButton close2;
	private PandaButton select_wl;
	private PandaButton select_kb;
	
	private JComboBox<String> suggestions;
	
	private JLabel currentMisspelledWord;
	private JLabel empty;
	private JLabel empty1;
	private JLabel empty2;
	private JLabel empty3;
	private JLabel wordlist;
	private JLabel keyboard;

	private boolean closedOuterPanel = false;
	private boolean isConfigureOpen = false;
	private boolean isSpellCheckerOpen = false;
	private boolean selectedWordListFile = false;
	private boolean selectedKeyboardFile = false;
	private boolean recentlySaved = false;
	
	private String filename;
	
	public JEditor(String text) {
		
		//create JTextArea
		jta = new JTextArea(text);	
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		jta.setForeground(Color.black);
		jta.setBackground(Color.white);
		jta.setSelectionColor(new Color(115, 230, 142));
		jsp = new JScrollPane(jta);
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);	
		d = jta.getDocument();
			
	}
	
	//creates JPanel that will displayed in the East of the main frame
	public void createOuterPanel(ArrayList<String> files) {
		
		//run spell checker
		spellchecker = new Autocorrect();
		String[] file_array = new String[files.size()];
		for(int i = 0; i < files.size(); i++) {
			file_array[i] = files.get(i);
		}
		spellchecker.readFiles(file_array);
		spellchecker.store_kb_input();
		spellchecker.fillTrie();
		wordIndex = 0;	
		recentlySaved = false;
		
		wlfile = spellchecker.getWordlist();
		
		errors = new ArrayList<String>();
		startIndices = new ArrayList<Integer>();
		endIndices = new ArrayList<Integer>();
		optionSet = new ArrayList<ArrayList<String> >();
		
		parseText();
		createJPanels();
		createJButtons();
		createJLabels();
		setLayouts();
		createComboBox();
		setPanelBorders();
		createFileChoosers();
		
		if(!outerPanel.isVisible()) {
			outerPanel.setVisible(true);
		}
		
		createspellCheckWindowLayout();
		createConfigureLayout();	
		addActions();
		jta.setEditable(false);
	}
	
	//Display a no error message if text  has no errors
	public void checkForErrors() {
		if(errors.size() == 0) {
			JOptionPane.showMessageDialog(null, "No Errors!", "No errores", JOptionPane.INFORMATION_MESSAGE);
			outerPanel.setVisible(false);
			closedOuterPanel = true;
			jta.setEditable(true);
		}
	}
	
	//parse through text of jta and search for errors
	public void parseText() {
		String text = jta.getText();
		text = text.replaceAll("\n", " ");
		text = text.replaceAll("\t", " ");
		
		Scanner scan = new Scanner(text);
		while(scan.hasNext()) {
			String s = scan.next();
			
			String clean = cleanString(s);
			
			//find top 5 suggestions of all errors
			if(spellchecker.isSpellingError(clean)) {
				spellchecker.findSolutions(clean);
				ArrayList<String> options = spellchecker.getSuggestions(5);
				
				MatchResult match = scan.match();
				int start = match.start();
				int end = match.end();
				
				optionSet.add(options);
				errors.add(clean);
				startIndices.add(start);
				endIndices.add(end);
			}
			
		}
		scan.close();		

	}
	
	//wipe string of punction and capitalization
	public String cleanString(String s) {
		String clean = "";
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				clean += (char) (s.charAt(i) + 32);
			}
			if(s.charAt(i) >= 'a' && s.charAt(i) <= 'z') {
				clean += s.charAt(i);
			}
		}
		return clean;
	}
	
	public void createJPanels() {
		outerPanel = new JPanel();
		outerPanel.setBackground(new Color(204, 255, 229));
		spellCheckWindow = new JPanel(); 
		configure = new JPanel();
		grid = new JPanel();
		row1 = new JPanel();
		row2 = new JPanel();
		box3 = new JPanel();
		
	}
	
	public void createJButtons() {
		ignore = new PandaButton("Ignore", "left");
		add = new PandaButton("  Add  ", "right");
		change = new PandaButton("Change", "right");
		close = new PandaButton("Close", "center");
		close2 = new PandaButton("Close", "left");
		select_wl = new PandaButton("Select WordList...", "config");
		select_kb = new PandaButton("Select Keyboard...", "config");
	}
	
	public void createJLabels() {
		empty = new JLabel(" ");
		empty1 = new JLabel(" ");
		empty2 = new JLabel(" ");
		empty3 = new JLabel(" ");
		wordlist = new JLabel("wordlist.wl");
		keyboard = new JLabel("qwerty-us.kb");
		
		if(errors.size() > 0) {
			currentMisspelledWord = new JLabel("Spelling: " + errors.get(0));		
			
		}
		else {
			currentMisspelledWord = new JLabel("xxx");
		}
		currentMisspelledWord.setFont(new Font("Papyrus", Font.PLAIN, 16));
		
	}
	
	public void setLayouts() {
		outerPanel.setLayout(new CardLayout());
		configure.setLayout(new BoxLayout(configure, BoxLayout.Y_AXIS));
		grid.setLayout(new GridLayout(3,1));
		box3.setLayout(new BorderLayout());
		row1.setLayout(new BoxLayout(row1, BoxLayout.X_AXIS));
		row2.setLayout(new BoxLayout(row2, BoxLayout.X_AXIS));
		spellCheckWindow.setLayout(new BoxLayout(spellCheckWindow, BoxLayout.Y_AXIS));
	}
	
	public void createComboBox() {
		//check if OptionSet is empty
		if(optionSet.size() == 0 ) {
			change.setEnabled(false);
			suggestions = new JComboBox<String>();
			
		}
		else if(optionSet.get(0).size() == 0) {
			change.setEnabled(false);
			suggestions = new JComboBox<String>();
		}
		else {
			String[] options = new String[5];
			for(int i = 0; i < optionSet.get(0).size(); i++) {
				options[i] = optionSet.get(0).get(i);
			}
			suggestions = new JComboBox<String>(options);
		}
		suggestions.setBackground(Color.white);
		suggestions.setUI(new MyComboBoxUI());
		
	}
	
	public void setPanelBorders() {

		spellCheckWindow.setBorder(BorderFactory.createTitledBorder("Spell Check"));
		configure.setBorder(BorderFactory.createTitledBorder("Configure"));
	}
	
	//create spell check gui
	public void createspellCheckWindowLayout() {
		
		
		spellCheckWindow.add(empty1);
		spellCheckWindow.add(currentMisspelledWord);
		currentMisspelledWord.setAlignmentX(Component.CENTER_ALIGNMENT);
		spellCheckWindow.add(empty);
		
		row1.add(ignore);
		row1.add(add);
		
		spellCheckWindow.add(row1);
		spellCheckWindow.add(empty3);
		
		row2.add(suggestions);
		row2.add(change);
		row2.setMaximumSize(new Dimension(400, 25));
		
		spellCheckWindow.add(row2);
		
		box3.add(close, BorderLayout.SOUTH);
		
		spellCheckWindow.setBackground(new Color(204, 255, 229));
		box3.setBackground(new Color(204, 255, 229));
		grid.setBackground(new Color(204, 255, 229));
		
		grid.add(spellCheckWindow);
		grid.add(empty2);
		grid.add(box3);
		
		outerPanel.add(grid, "SpellCheck");
		
	}
	
	//create configure gui
	public void createConfigureLayout() {
		
		configure.setBackground(new Color(204, 255, 229));
		configure.add(wordlist);
		configure.add(select_wl);	
		
		configure.add(Box.createRigidArea(new Dimension(10,20)));
		
		configure.add(keyboard);
		configure.add(select_kb);
			
		configure.add(Box.createGlue());
		configure.add(close2);	
		
		outerPanel.add(configure, "Configure");
	}
	
	public void createFileChoosers() {
		jfc_kbfiles = new JFileChooser();
		FileNameExtensionFilter filter_kb = new FileNameExtensionFilter("kb files (*.kb)", "kb");
		jfc_kbfiles.setFileFilter(filter_kb);
		
		jfc_wlfiles = new JFileChooser();
		FileNameExtensionFilter filter_wl = new FileNameExtensionFilter("wl files (*.wl)", "wl");
		jfc_wlfiles.setFileFilter(filter_wl);
	}
	
	public void addActions() {
		//move to next word
		ignore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextWord();
			}
		});
		
		//add to dictionary and move to next word
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String word = errors.get(wordIndex);
				word = cleanString(word);
	
				appendToWordList(word);
				
				while(errors.contains(word)) {
					int i = errors.indexOf(word);
					errors.remove(i);
					startIndices.remove(i);
					endIndices.remove(i);
					optionSet.remove(i);				
				}
				wordIndex--;
				nextWord();
			}
		});
		
		//change error to suggested word
		change.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String current = "";
				int start = startIndices.get(wordIndex);
				int end = endIndices.get(wordIndex);
				try {
					current = jta.getText(start, end - start);
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}
				String replace = (String) suggestions.getSelectedItem();
				jta.replaceRange(replace, start, end);
				updateIndices(replace.length() - current.length());
				nextWord();
			}
		});
		
		//close spell check window
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outerPanel.setVisible(false);
				closedOuterPanel = true;
				isSpellCheckerOpen = false;
				jta.setEditable(true);
			}
		});
		
		//close configure window
		close2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outerPanel.setVisible(false);
				closedOuterPanel = true;
				isConfigureOpen = false;
				jta.setEditable(true);
			}
		});
		
		//select a new word list file in jfilechooser
		select_wl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int returnVal = jfc_wlfiles.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					wlfile = jfc_wlfiles.getSelectedFile();
					closedOuterPanel = true;
					selectedWordListFile = true;
				}
			}
		});
		
		//select a new key board file in jfilechoooser
		select_kb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int returnVal = jfc_kbfiles.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					kbfile = jfc_kbfiles.getSelectedFile();
					closedOuterPanel = true;
					selectedKeyboardFile = true;
				}
			}
		});
	}
	
	//move to next word
	public void nextWord() {
		wordIndex++;
		if(wordIndex < errors.size()) {
			focusSpellingError(wordIndex);
			currentMisspelledWord.setText("Spelling: " + errors.get(wordIndex));
			suggestions.removeAllItems(); 
			if(optionSet.get(wordIndex).size() == 0) {
				change.setEnabled(false);
			}
			else {
				change.setEnabled(true);
				for(int i = 0; i < optionSet.get(wordIndex).size(); i++) {
					suggestions.addItem(optionSet.get(wordIndex).get(i));
				}
			}
		}
		else {
			JOptionPane.showMessageDialog(null, "All Words have been checked!", 
					"Spell Check Complete", JOptionPane.INFORMATION_MESSAGE);
			outerPanel.setVisible(false);
			closedOuterPanel = true;
			jta.setEditable(true);
		}
	}
	
	//select word during spell check process
	public void focusSpellingError(int i) {
		int indexStart = startIndices.get(i);
		int indexEnd = endIndices.get(i);
		jta.requestFocus();
		jta.setCaretPosition(indexStart);
		jta.setSelectionStart(indexStart);
		jta.setSelectionEnd(indexEnd);
	}
	
	//update indice arrays based on changed words
	public void updateIndices(int change) {
		for(int i = 0; i < startIndices.size(); i++) {
			int previous = startIndices.get(i);
			startIndices.set(i, previous + change);
			
			int previous2 = endIndices.get(i);
			endIndices.set(i, previous2 + change);
		}
	}
	
	//add word to word list file
	public void appendToWordList(String word) {
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(wlfile, true)))) {
		    out.print("\n" + word);
		}catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	public JPanel getOuterPanel() {
		return outerPanel;
	}
	
	public boolean closedOuterPanel() {
		return closedOuterPanel;
	}
	
	public JTextArea getJTA() {
		return jta;
	}

	public JScrollPane getJSP() {
		return jsp;
	}
	
	public File getFile() {
		return f;
	}
	
	public void setFile(File f) {
		this.f = f;
	}
	
	public Document getDocument() {
		return d;
	}
	
	public int getNumErrors() {
		return errors.size();
	}

	public boolean selectedKeyboardFile() {
		return selectedKeyboardFile;
	}
	
	public boolean selectedWordListFile() {
		return selectedWordListFile;
	}
	public File getSelectedWordListFile() {
		return wlfile;
	}
	
	public File getSelectedKeyBoardFile() {
		return kbfile;
	}
	
	public boolean isConfigureOpen() {
		return isConfigureOpen;
	}
	
	public boolean isSpellCheckerOpen() {
		return isSpellCheckerOpen;
	}
	
	public void closeOuterPanel() {
		outerPanel.setVisible(false);
		closedOuterPanel = true;
		jta.setEditable(true);
	}
	
	public void setIsConfigureOpen(boolean b) {
		isConfigureOpen = b;
	}
	
	public void setIsSpellCheckerOpen(boolean b) {
		isSpellCheckerOpen = b;
	}
	
	public void setRecentlySaved(boolean b) {
		recentlySaved = b;
	}
	
	public boolean getRecentlySaved() {
		return recentlySaved;
	}

	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
}
