package panda;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.AddUser;

public class AddUserGui extends JDialog{
	
	private static final long serialVersionUID = 1L;
	private JLabel addUserLabel;
	private JTextField inputField;
	private JButton okButton;
	private JButton cancelButton;
	private PandaGui panda;
	
	public AddUserGui(PandaGui panda) {
		this.panda = panda;
		instantiateComponents();
		createGui();
		addActions();
		this.setVisible(true);
	}

	private void instantiateComponents() {
		addUserLabel = new JLabel("Add User:");
		inputField = new JTextField();
		okButton = new JButton("Ok");
		cancelButton = new JButton("Cancel");
	}

	private void createGui() {
		this.setTitle("Add Users");
		this.setSize(200, 100);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.add(addUserLabel, BorderLayout.NORTH);
		
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(1,1));
		center.add(inputField);
		this.add(center, BorderLayout.CENTER);
		
		JPanel south = new JPanel();
		south.setLayout(new GridLayout(1,2));
		south.add(okButton);
		south.add(cancelButton);
		this.add(south, BorderLayout.SOUTH);
	}

	private void addActions() {
		okButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String username = inputField.getText();				
				if(panda.getPandaClientListener().isConneceted()) {
					AddUser adduser = new AddUser(username, panda.getUsername(), panda.getFilename());
					panda.getPandaClientListener().sendMessage(adduser);
				}
				
				dispose();
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		
		});
		
	}
	
}
