package panda;

import javax.swing.UIManager;

public class PandaMain {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		
		catch (Exception e) {
			System.out.println("Warning! Cross Platform L&F not used!");
		}
		
		//Default
		String[] files = {"src/qwerty-us.kb", "src/wordlist.wl"};
		
		PandaGui te = new PandaGui(files);
		te.setVisible(true);
		
	}
}
