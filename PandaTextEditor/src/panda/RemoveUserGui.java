package panda;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import client.RemoveUser;

public class RemoveUserGui extends JDialog{

	private static final long serialVersionUID = 1L;
	private JButton remove;

	public RemoveUserGui(PandaGui panda, String[] usersWithAccess, String filename) {
		this.setTitle("Choose a user:");
		this.setSize(200, 250);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		JTextField inputTextField = new JTextField();
		inputTextField.setEditable(false);
		
		JList<String> mlist = new JList<String>(usersWithAccess);
		JScrollPane jsp = new JScrollPane(mlist);
		jsp.setPreferredSize(new Dimension(100,150));
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		this.add(jsp, BorderLayout.CENTER);
				
		remove = new JButton("Remove");
		this.add(remove, BorderLayout.SOUTH);
		
		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				RemoveUser r = new RemoveUser(filename, mlist.getSelectedValue(), panda.getUsername());
				panda.getPandaClientListener().sendMessage(r);
				dispose();
			}
			
		});
		
		this.setVisible(true);
		
	}
}
