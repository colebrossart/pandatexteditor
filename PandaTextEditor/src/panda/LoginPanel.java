package panda;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import client.LoginInfo;
import client.PandaClientListener;


public class LoginPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private ImageIcon logo;
	private JLabel pandaLabel;
	
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	
	private JTextField usernameText;
	private JPasswordField passwordText;
	
	private PandaButton login;
	
	private String username;
	private char[] password;
	private String passwordString;
	private PandaClientListener pandaClientListener;

	public LoginPanel(PandaClientListener pcl) {
		this.pandaClientListener = pcl;
		instantiateComponents();
		createGUI();
		addActions();
		
	}

	private void instantiateComponents() {
		logo = new ImageIcon("img/logo2.jpg");
		pandaLabel = new JLabel(logo);
		
		usernameLabel = new JLabel("Username: ");
		passwordLabel = new JLabel("Password: ");
		
		usernameText = new JTextField();
		passwordText = new JPasswordField();
		
		login = new PandaButton("Login", "center");
		username = "";
		passwordString = "";
		
	}
	
	private void createGUI() {
		this.setBackground(Color.white);
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0, 0, 20, 0);
		this.add(pandaLabel, gbc);
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		this.add(usernameLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 120;
		this.add(usernameText, gbc);
		gbc.ipadx = 0;
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(passwordLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 120;
		this.add(passwordText, gbc);
		gbc.gridy = 4;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.ipadx = 50;
		gbc.insets = new Insets(20, 0, 0, 0);
		this.add(login, gbc);
	}
	
	private void addActions() {
		login.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				username = usernameText.getText();
				password = passwordText.getPassword();
				encryptPassword(password);
				
				if(pandaClientListener.connect()) { 
					LoginInfo li = new LoginInfo(username, passwordString);
					pandaClientListener.sendMessage(li);
				}
				passwordString = "";
				
			}	
		});
	}
	
	public void encryptPassword(char[] password) {
		
		for(int i = 0; i < password.length; i++) {
			if(password[i] >= '0' && password[i] <= '9') {
				char c = password[i] += 5;
				if(c > '9') {
					c -= 9;
				}
				passwordString += c;
			} else if(password[i] >= 'A' && password[i] <= 'Z') {
				char c = password[i] += 13;
				if(c > 'Z') {
					c -= 26;
				}
				passwordString += c;
			} else {
				char c = password[i] += 13;
				if(c > 'z') {
					c -= 26;
				}
				passwordString += c;
			}
		}
	}

	
}
