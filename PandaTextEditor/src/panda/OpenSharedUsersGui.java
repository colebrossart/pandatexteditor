package panda;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import client.OpenSharedFile;

public class OpenSharedUsersGui extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private JButton myFilesButton;
	private JButton selectUserButton;
	
	public OpenSharedUsersGui(PandaGui panda, String[] collabUsers) {
		this.setTitle("Choose a User: ");
		this.setSize(220, 250);
		this.setResizable(false);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		myFilesButton = new JButton("My Files...");
		selectUserButton = new JButton("Select User...");
		JList<String> mlist = new JList<String>(collabUsers);
		JScrollPane jsp = new JScrollPane(mlist);
		jsp.setPreferredSize(new Dimension(100,150));
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		myFilesButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new OpenFileDialog(panda.getUsername(), panda);
				dispose();
			}
			
		});
		
		
		selectUserButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				OpenSharedFile osf = new OpenSharedFile(panda.getUsername(), mlist.getSelectedValue());
				panda.getPandaClientListener().sendMessage(osf);
				dispose();
			}
			
		});
		
		this.add(jsp, BorderLayout.CENTER);
		
		JPanel jp = new JPanel();
		jp.setLayout(new GridLayout(1,2));
		
		jp.add(myFilesButton);
		jp.add(selectUserButton);
		
		this.add(jp, BorderLayout.SOUTH);
		
		setVisible(true);
		
		
	}

}
