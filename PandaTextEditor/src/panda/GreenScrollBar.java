package panda;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;


//http://stackoverflow.com/questions/12265740/make-a-custom-jscrollbar-using-an-image
//https://community.oracle.com/thread/2211444?tstart=0

public class GreenScrollBar extends BasicScrollBarUI{
	
	private Image up;
	private Image down;
	private Image track;
	private Image thumb;
	
	public GreenScrollBar() {
		try {
			down = ImageIO.read(new File("img/down.png"));
			up = ImageIO.read(new File("img/up.png"));
			track = ImageIO.read(new File("img/track.png"));
			thumb = ImageIO.read(new File("img/thumb.png"));
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}
	
	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		g.translate(trackBounds.x, trackBounds.y);
        ((Graphics2D)g).drawImage(track,AffineTransform.getScaleInstance
        		(1,(double)trackBounds.height/track.getHeight(null)),null);
        g.translate( -trackBounds.x, -trackBounds.y );  
	}
	@Override
	protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
	    g.translate(thumbBounds.x, thumbBounds.y);        
        AffineTransform transform = AffineTransform.getScaleInstance
        		((double)thumbBounds.width/thumb.getWidth(null),(double)thumbBounds.height/thumb.getHeight(null));
        ((Graphics2D)g).drawImage(thumb, transform, null);
        g.translate( -thumbBounds.x, -thumbBounds.y );
	}
	
	protected JButton createDecreaseButton(int orientation) {
           JButton button = new JButton(new ImageIcon(down));
           button.setBackground( new Color( 255,255,255));
           button.setForeground( new Color( 255,255,255));
           button.setBorderPainted(false);
           return button;
      }

      @Override
      protected JButton createIncreaseButton(int orientation) {
           JButton button = new JButton(new ImageIcon(up));
           button.setBackground( new Color(255,255,255));
           button.setForeground( new Color(255,255,255));
           button.setBorderPainted(false);
           return button;
      }
}
