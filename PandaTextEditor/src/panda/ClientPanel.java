package panda;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

//possibly pass in instance of PandaFrame

public class ClientPanel extends JPanel{
	
	private PandaButton login;
	private PandaButton signup;
	private PandaButton offline;
	private ImageIcon logo; 
	private JLabel pandaLabel;
	private ImageIcon logoFlip; 
	private JLabel pandaLabelFlip;
	
	private static final long serialVersionUID = 1L;
	
	public ClientPanel() {
		instantiateComponents();
		createGUI();
	}
	
	public void instantiateComponents() {
		login = new PandaButton("    Login", "left");
		signup = new PandaButton("Sign up", "right");
		offline = new PandaButton("Offline", "center");
		logo = new ImageIcon("img/logo2.jpg");
		pandaLabel = new JLabel(logo);
		logoFlip = new ImageIcon("img/logoFlip.jpeg");
		pandaLabelFlip = new JLabel(logoFlip);
	}
	
	public void createGUI() {
		this.setBackground(Color.white);
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 20 , 0);
		gbc.gridwidth = 2;
		this.add(pandaLabel, gbc);	
		gbc.ipadx = 40;
		gbc.ipady = 10;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(10, 10, 0 , 0);
		gbc.gridy = 1;
		this.add(login, gbc);
		gbc.gridx++;
		this.add(signup, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.ipadx = 100;
		gbc.gridwidth = 2;
		this.add(offline, gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.insets = new Insets(20, 10, 0 , 0);
		this.add(pandaLabelFlip, gbc);

	}
	
	public PandaButton getLoginButton() {
		return login;
	}
	
	public PandaButton getSignupButton() {
		return signup;
	}
	
	public PandaButton getOfflineButton() {
		return offline;
	}

}
