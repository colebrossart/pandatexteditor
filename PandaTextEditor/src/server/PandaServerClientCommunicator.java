package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import client.AddUser;
import client.CloseFile;
import client.CreateUserPath;
import client.GoOffline;
import client.HasAccess;
import client.LoginInfo;
import client.MergeFile;
import client.OpenCollabUsers;
import client.OpenFile;
import client.OpenSharedFile;
import client.RemoveUser;
import client.RequestSharedUsers;
import client.SaveFile;
import client.GetOpenFiles;
import client.SigninInfo;
import client.UpdateOpenFiles;


public class PandaServerClientCommunicator extends Thread {

	private Socket socket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private PandaServerListener serverListener;
	private PandaJDBC database;
	
	public PandaServerClientCommunicator(Socket socket, PandaServerListener serverListener) throws IOException {
		this.socket = socket;
		this.serverListener = serverListener;
		this.oos = new ObjectOutputStream(socket.getOutputStream());
		this.ois = new ObjectInputStream(socket.getInputStream());
		this.database = new PandaJDBC();
	}
	
	public void setOffline() {	
		try {
			GoOffline go = new GoOffline(true);
			oos.writeObject(go);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public void run() {
		try {
			
			while(true) {
				Object o = ois.readObject(); 
				
				if(o instanceof SigninInfo) {
					SigninInfo si = (SigninInfo)o;
					PandaServerGUI.addMessage("Sign-up Attempt User: " + si.getUsername() + " Password: " + si.getPassword());
					if(database.signinAttempt(si.getUsername(), si.getPassword())) {
						PandaServerGUI.addMessage("Sign-up Success User: " + si.getUsername());
						si.setSuccess(true);
						oos.writeObject(si);
						oos.flush();
					} else {
						PandaServerGUI.addMessage("Sign-up Failure User: " + si.getUsername());
						oos.writeObject(si);
						oos.flush();
					}
				} else if(o instanceof LoginInfo){
					LoginInfo li = (LoginInfo) o;
					PandaServerGUI.addMessage("Log-in Attempt User: " + li.getUsername() + " Password: " + li.getPassword());
					if(database.loginAttempt(li.getUsername(), li.getPassword())) {
						PandaServerGUI.addMessage("Log-in Success User: " + li.getUsername());
						li.setSuccess(true);
						oos.writeObject(li);
						oos.flush();
					} else {
						PandaServerGUI.addMessage("Log-in Failure User: " + li.getUsername());
						oos.writeObject(li);
						oos.flush();
					}
				} else if(o instanceof CreateUserPath) {
					CreateUserPath cup = (CreateUserPath) o;
					File f = new File("users/" + cup.getUsername() + ".txt");
					f.createNewFile();
					File dir = new File("files/" + cup.getUsername());	
					dir.mkdir();
					
				} else if(o instanceof SaveFile) {
					
						
						SaveFile sf = (SaveFile) o;
						
						serverListener.getLock().lock();
						database.saveFile(sf.getFilename(), sf.getUsername());
						
						BufferedWriter bw = null;				
						File f = new File("files/" + sf.getUsername() + "/"+ sf.getFilename());
						if(f.createNewFile()) {
							try {
								Files.write(Paths.get("users/" + sf.getUsername() + ".txt"), 
									(sf.getFilename() + "\n").getBytes(), 
						    		StandardOpenOption.APPEND);
							} catch (IOException ioe) {
								ioe.printStackTrace();
							}
							if(serverListener.getOpenFileTracker().containsKey(f.getAbsolutePath())) {
						    	Integer i = serverListener.getOpenFileTracker().get(f.getAbsoluteFile());
						    	serverListener.getOpenFileTracker().put(f.getAbsolutePath(), new Integer(i.intValue() + 1));
						    	
						    } else {
						    	serverListener.getOpenFileTracker().put(f.getAbsolutePath(), new Integer(1));
						    
							}
							
							
						} else {
							sf.setReplace(true);
						}
						
						
						
						if(!sf.getReplace()) {
							
							try {
								
								bw = new BufferedWriter(new FileWriter("files/" + sf.getUsername() + "/"+ sf.getFilename()));
								bw.write(sf.getText());
								bw.close();
							} catch (IOException e1) {
								e1.printStackTrace();
							} finally {
								
							}
						}
						
						
						
						PandaServerGUI.addMessage("File Saved User: "+ sf.getUsername() + 
								" File: " + sf.getFilename());
						if(!sf.getAutoSave()) {	
							oos.writeObject(sf);
							
						} 
						serverListener.getLock().unlock();
					
				} else if (o instanceof OpenFile) {
					
					
						OpenFile of = (OpenFile) o;
						serverListener.getLock().lock();
					
						try {
							File f = new File("files/" + of.getUsername() + "/"+ of.getFilename());
					    	 if(serverListener.getOpenFileTracker().containsKey(f.getAbsolutePath())) {
							    	Integer i = serverListener.getOpenFileTracker().get(f.getAbsolutePath());
							    	serverListener.getOpenFileTracker().put(f.getAbsolutePath(), new Integer(i.intValue() + 1));
							    	
							    } else {
							    	serverListener.getOpenFileTracker().put(f.getAbsolutePath(), new Integer(1));
						    }
					    	
							
							BufferedReader br = null;
							try {
						    	
						    	br = new BufferedReader(new FileReader(f));
							    StringBuilder sb = new StringBuilder();
							    String line = br.readLine();
	
							    while (line != null) {
							        sb.append(line);
							        sb.append(System.lineSeparator());
							        line = br.readLine();
							    }
							    of.setText(sb.toString());
							    oos.writeObject(of);
							    oos.flush();
							} finally {
							    br.close();
							    
							}
						} catch (FileNotFoundException fnfe) {
							fnfe.printStackTrace();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
						PandaServerGUI.addMessage("File Opened User: "+ of.getUsername() + 
								" File: " + of.getFilename());
						serverListener.getLock().unlock();
					
				} else if(o instanceof AddUser) {
					AddUser a = (AddUser)o;
					if(!database.addUserAttempt(a.getUsername(), a.getOwner(), a.getFilename())) {
						//let client know it failed
						oos.writeObject(a);
						oos.flush();
					}
				} else if(o instanceof RequestSharedUsers) {
					RequestSharedUsers r = (RequestSharedUsers)o;
					r.setUsersWithAccesss(database.getUsersWithAccess(r.getFilename(), r.getOwner()));
					oos.writeObject(r);
					oos.flush();
				} else if(o instanceof RemoveUser) {
					RemoveUser r = (RemoveUser)o;
					database.removeSharedUser(r.getFilename(), r.getUsername(), r.getOwnername());
				} else if(o instanceof OpenCollabUsers) {
					OpenCollabUsers ocu = (OpenCollabUsers) o;
					ocu.setCollabUsers(database.getCollabUsers(ocu.getUsername()));
					oos.writeObject(ocu);
					oos.flush(); 
				} else if(o instanceof OpenSharedFile) {
					OpenSharedFile osf = (OpenSharedFile) o;
					osf.setAccessibleFiles(database.getAccessibleFiles(osf.getUsername(), osf.getOwner()));
					oos.writeObject(osf);
					oos.flush();
				} else if(o instanceof HasAccess) {
					HasAccess ha = (HasAccess) o;
					if(!database.hasAccess(ha.getFilename(), ha.getUsername(), ha.getOwner())) {
						oos.writeObject(ha);
						oos.flush();
					}
				} else if(o instanceof MergeFile) {
					MergeFile mf = (MergeFile) o;
					serverListener.addMergeFile(mf);
				} else if(o instanceof CloseFile) {
					CloseFile cf = (CloseFile) o;
				    	Integer i = serverListener.getOpenFileTracker().get(cf.getFilePath());
				    	serverListener.getOpenFileTracker().put(cf.getFilePath(), new Integer(i.intValue() - 1));
				}
			}
			
		} catch(ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage()); 
		} catch (IOException ioe) {
			serverListener.removeServerClientCommunicator(this);
			// this means that the socket is closed since no more lines are being received
			try {
				socket.close();
			} catch (IOException ioe1) {
				System.out.println("ioe: " + ioe1.getMessage());
			}
		}
	}

	

	public void saveOpenFiles() {
		//send message to client to save open files
		GetOpenFiles gof = new GetOpenFiles();
		try {
			oos.writeObject(gof);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public void updateOpenFiles() {
		UpdateOpenFiles uof = new UpdateOpenFiles();
		try {
			oos.writeObject(uof);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public ObjectOutputStream getObjectOutputStream() {
		return oos;
	}

	
}
