package server;

import java.net.ServerSocket;

public class PandaServer {
	
	private PandaServerGUI psg;
	private ServerSocket ss;
	private static PandaServerListener serverListener;

	
	public PandaServer()
	{
		psg = new PandaServerGUI(this);	
	}
	
	
	public void listenForConnections(int updateInterval) {
		ss = psg.getServerSocket();
		serverListener = new PandaServerListener(ss, updateInterval);
		serverListener.start();
	}
	
	public void closeSocket() {
		serverListener.closeSocket();
	}
	
	public PandaServerListener getServerListener() {
		return serverListener;
	}

	public static void main(String[] args) {
		new PandaServer();
	}


	
}
