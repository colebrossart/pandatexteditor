package server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import client.MergeFile;
import name.fraser.neil.plaintext.diff_match_patch;
import name.fraser.neil.plaintext.diff_match_patch.Patch;

public class PandaServerListener extends Thread {

	private ServerSocket ss;
	private Vector<PandaServerClientCommunicator> psccVector;
	private Socket s;
	private AutoSaveThread autoSaveThread;
	private UpdateThread updateThread;
	private Vector<MergeFile> serverMergeFiles;
	private HashMap<String, Integer> openFileTracker;
	private HashMap<String, Integer> mergeFileTracker;
	private Lock lock = new ReentrantLock();

	
	public PandaServerListener(ServerSocket ss, int updateInterval) {
		this.ss = ss;
		psccVector = new Vector<PandaServerClientCommunicator>();
		autoSaveThread = new AutoSaveThread(psccVector);
		updateThread = new UpdateThread(psccVector, updateInterval);
		this.serverMergeFiles = new Vector<MergeFile>();
		this.openFileTracker = new HashMap<String, Integer>();
		this.mergeFileTracker = new HashMap<String, Integer>();
		
	}
	
	public void removeServerClientCommunicator(PandaServerClientCommunicator scc) {
		psccVector.remove(scc);
	}
	
	public void run() {	
		try {
			while(true) {				
				s = ss.accept();
				try {
					PandaServerClientCommunicator pscc = new PandaServerClientCommunicator(s, this);
					pscc.start();
					psccVector.add(pscc);
					
				} catch (IOException ioe) {
					System.out.println("server listenrer ioe: " + ioe.getMessage());
				}
			}
		} catch (BindException be) {
			System.out.println("be" + be.getMessage());
		} catch (IOException ioe) {
			if(ss != null) {
				try {
					ss.close();
				} catch (IOException ioe2) {
					System.out.println("server listener ioe: " + ioe2.getMessage());	
				}
			}
		} 

	}

	public void closeSocket() {
		
		for(int i = 0; i < psccVector.size(); i++) {
			psccVector.get(i).setOffline();
		}
		
		try {
			if(s != null) {
				s.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void addMergeFile(MergeFile mf) {
		boolean update = false;
		for(int i = 0; i < serverMergeFiles.size(); i++) {
			if(mf.getFilePath().equals(serverMergeFiles.get(i).getFilePath()) &&
					mf.getClient() == serverMergeFiles.get(i).getClient()) {
					serverMergeFiles.get(i).setOriginalText(mf.getOriginalText());
					serverMergeFiles.get(i).setUpdatedText(mf.getUpdatedText());
					update = true;
			} 	
		}
		if(!update) {
			serverMergeFiles.add(mf);	
			if(mergeFileTracker.containsKey(mf.getFilePath())) {
		    	Integer i = mergeFileTracker.get(mf.getFilePath());
		    	mergeFileTracker.put(mf.getFilePath(), new Integer(i.intValue() + 1));
		    	
		    } else {
		    	mergeFileTracker.put(mf.getFilePath(), new Integer(1));
		    }
		}
		
		if(mergeFileTracker.get(mf.getFilePath()).equals(openFileTracker.get(mf.getFilePath()))) {
			mergeFileTracker.put(mf.getFilePath(), new Integer(0));
			mergeFiles(mf);
		}
	}
	
	public void mergeFiles(MergeFile mf) {
		
		lock.lock();
		diff_match_patch dmp = new diff_match_patch();
		String originaltext = mf.getOriginalText();
		LinkedList<Patch> patch;
		String merge = mf.getOriginalText();
		Vector<MergeFile> mergeFiles = new Vector<MergeFile>();
		
		for(int i = 0; i < serverMergeFiles.size(); i++) {
			if(serverMergeFiles.get(i).getFilePath().equals(mf.getFilePath())){
				patch = dmp.patch_make(originaltext, serverMergeFiles.get(i).getUpdatedText());
				merge = (String)(dmp.patch_apply(patch, merge)[0]);
				mergeFiles.addElement(serverMergeFiles.get(i));
			}
		}
		
		for(int i = 0; i < mergeFiles.size(); i++) {
			mergeFiles.get(i).setUpdatedText(merge);
		}
		
		try {
			for(int i = 0; i < psccVector.size(); i++) {
				for(int j = 0; j < mergeFiles.size(); j++) {
					psccVector.get(i).getObjectOutputStream().writeObject(mergeFiles.get(j));
					psccVector.get(i).getObjectOutputStream().flush();
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < mergeFiles.size(); i++) {
			serverMergeFiles.remove(mergeFiles.get(i));
		}
		
		lock.unlock();
	}
	
	public AutoSaveThread getTimerThread() {
		return autoSaveThread;
	}
	
	public UpdateThread getUpdateThread() {
		return updateThread;
	}
	
	public Vector<PandaServerClientCommunicator> getpsccVector() {
		return psccVector;
	}
	
	public Vector<MergeFile> getServerMergeFiles() {
		return serverMergeFiles;
	}

	public HashMap<String, Integer> getOpenFileTracker() {
		return openFileTracker;
	}

	public HashMap<String, Integer> getMergeFileTracker() {
		return mergeFileTracker;
	}

	public Lock getLock() {
		return lock;
	}
}
