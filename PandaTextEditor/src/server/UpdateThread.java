package server;

import java.util.Vector;

public class UpdateThread extends Thread {
	
	private Vector<PandaServerClientCommunicator> psccVector;
	private int updateInterval;
	
	public UpdateThread(Vector<PandaServerClientCommunicator> psccVector, int updateInterval) {
		this.psccVector = psccVector;
		this.updateInterval = updateInterval;
	}
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(updateInterval);			
			} catch (InterruptedException e) {
				break;
			}
			for(int i = 0; i < psccVector.size(); i++) {
				psccVector.get(i).updateOpenFiles();
			}
		}
	}
}
