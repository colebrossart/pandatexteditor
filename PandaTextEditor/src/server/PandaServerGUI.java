package server;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import panda.GreenScrollBar;
import panda.PandaButton;

public class PandaServerGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private static JTextArea log;
	private JScrollPane jsp;
	private PandaButton startStopButton;
	private boolean isStart;
	private ServerSocket ss;
	private int portNumber;
	private PandaServer pandaServer;
	private int updateInterval;
	
	public PandaServerGUI(PandaServer pandaServer) {
		super("Server");
		this.pandaServer = pandaServer;
		instantiateComponents();
		createGUI();
		portNumber = readServerConfig();
		addActions();
		this.setVisible(true); 
		
	}
	
	private void instantiateComponents() {
		log = new JTextArea();
		log.setEditable(false);
		jsp = new JScrollPane(log);
		jsp.getVerticalScrollBar().setUI(new GreenScrollBar());
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		startStopButton = new PandaButton("Start", "server");
		startStopButton.setFont(new Font("Papyrus", Font.PLAIN, 14));
		isStart = true;
		ss = null;
		
		
	}
	
	public void createGUI() {
		this.setLocation(10, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 400);
		this.add(jsp, BorderLayout.CENTER);
		this.add(startStopButton, BorderLayout.SOUTH);
	}
	
	private void addActions() {
		startStopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {

				if(isStart) {
					log.append("Server started on Port: " + portNumber + "\n");
					startStopButton.setText("Stop");
					isStart = false;				
					
					try {
						ServerSocket tempss = new ServerSocket(portNumber);
						ss = tempss;
						pandaServer.listenForConnections(updateInterval);
					} catch (IOException ioe) {
						System.out.println("start ioe: " + ioe.getMessage());
					}
					
					if(!pandaServer.getServerListener().getTimerThread().isAlive()) {
						pandaServer.getServerListener().getTimerThread().start();
					}
					if(!pandaServer.getServerListener().getUpdateThread().isAlive()) {
						pandaServer.getServerListener().getUpdateThread().start();
					}
					
				}
				else {
					log.append("Server stopped." + "\n");
					startStopButton.setText("Start");
					pandaServer.closeSocket();
								
					pandaServer.getServerListener().getTimerThread().interrupt();
					pandaServer.getServerListener().getUpdateThread().interrupt();
					
					isStart = true;
					try {
						ss.close();
					} catch (IOException e) {
						System.out.println("stop ioe: " + e.getMessage());
					}
					
				}
				
			}
		});
	}
	
	//returns port number and update interval from Server.Config file 
	private int readServerConfig() {
		int port = 0;
		Scanner scan = null;
		try {
			scan = new Scanner(new File("src/Server.config"));
			String s = scan.next();
			if(s.equals("Port:")) {
				port = scan.nextInt();
			}
			String s2 = scan.next();
			if(s2.equals("UpdateInterval:")) {
				updateInterval = scan.nextInt();
			}
			
		} catch (IOException ioe){
			System.out.println("read config ioe: " + ioe.getMessage());
		}
		scan.close();
		
		return port;
	}
	
	public ServerSocket getServerSocket() {
		return ss;
	}

	public static void addMessage(String msg) {
		if (log.getText() != null && log.getText().trim().length() > 0) {
			log.append(msg + "\n");
		}
		else {
			log.setText(msg);
		}
	}
	
}
