	package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class PandaJDBC {
	
	private Connection conn;

	public PandaJDBC() {
		conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306"
					+ "/cbrossar?user=root&password=cole0362&useSSL=false");

		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe: " + cnfe.getMessage());
		} 
		
	}
	
	public boolean signinAttempt(String username, String password) {
		
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT u.username FROM UserInfo u");
			while(rs.next()) {
				String tmpUsername = rs.getString("username");
				if(tmpUsername.equals(username)) {
					return false;
				}
			}
			st.executeUpdate("INSERT INTO UserInfo (username, pword) "
					+ "VALUES ('" + username + "', '" + password + "')");
			
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		return true;
	}
	
	public boolean loginAttempt(String username, String password) {
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT u.username, u.pword FROM UserInfo u");
			while(rs.next()) {
				String tmpUsername = rs.getString("username");
				String tmpPassword = rs.getString("pword");
				if(tmpUsername.equals(username) && tmpPassword.equals(password)) {
					return true;
				}
			}
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		return false;
	}
	
	public boolean addUserAttempt(String username, String ownerName, String filename) {
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT u.username FROM UserInfo u");
			while(rs.next()) {
				String tmpUsername = rs.getString("username");
				if(tmpUsername.equals(username)) {
					String[] usersWithAccess = getUsersWithAccess(filename, ownerName);
					for(int i = 0; i < usersWithAccess.length; i++) {
						if(usersWithAccess[i].equals(username)) {
							return true;
						}
					}
					st.executeUpdate("INSERT INTO FileShare(fileID, sharedUser) VALUES "
							+ "((SELECT fileID FROM FileTable WHERE "
							+ "ownerName = '" + ownerName + "' AND "
							+ "filename = '" + filename + "'), '" + username + "')");
					return true;
				}
			}
			return false;
				
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		return false;
	}

	public void saveFile(String filename, String username) {
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT fileName, ownerName FROM FileTable");
			while(rs.next()) {
				String tmpUsername = rs.getString("ownerName");
				String tmpFileName = rs.getString("fileName");
				if(tmpUsername.equals(username) && tmpFileName.equals(filename)) {
					return;
				}
			}
			st.executeUpdate("INSERT INTO FileTable(fileName, ownerName) VALUES "
					+ "('" + filename + "' , '" + username + "' )");
				
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
	}

	public String[] getUsersWithAccess(String filename, String owner) {
		ArrayList<String> userList = new ArrayList<String>();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT sharedUser FROM FileShare "
					+ " WHERE fileID = (SELECT fileID FROM FileTable WHERE "
					+ "fileName = '" + filename + "' AND "
					+ "ownerName = '" + owner + "')");
			while(rs.next()) {
				String tmpUsername = rs.getString("sharedUser");
				userList.add(tmpUsername);
			}
				
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		
		String[] userArray = new String[userList.size()];
		userArray = userList.toArray(userArray);
		return userArray;
	}

	public void removeSharedUser(String filename, String username, String ownername) {
		try {
			Statement st = conn.createStatement();
			st.executeUpdate("DELETE FROM FileShare WHERE "
					+ "sharedUser = '" + username + "' AND fileID = "
							+ "(SELECT fileID FROM FileTable "
							+ "WHERE fileName = '" + filename + "' AND "
									+ "ownerName = '" + ownername + "')");
							
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
	}

	public String[] getCollabUsers(String username) {
		Set<String> collabUserSet = new HashSet<String>();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT ft.ownerName FROM FileTable ft, FileShare fs "
					+ "WHERE fs.sharedUser = '" + username + "' AND "
							+ "fs.fileID = ft.fileID");
			while(rs.next()) {
				String tmpUsername = rs.getString("ownerName");
				collabUserSet.add(tmpUsername);
			}
				
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		
		String[] collabUserArray = new String[collabUserSet.size()];
		collabUserArray = collabUserSet.toArray(collabUserArray);
		return collabUserArray;
	}

	public String[] getAccessibleFiles(String username, String owner) {
		ArrayList<String> fileList = new ArrayList<String>();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT ft.fileName FROM FileTable ft, FileShare fs WHERE "
					+ "ft.ownerName = '" + owner + "' AND "
					+ "ft.fileID = fs.fileID AND "
					+ "fs.sharedUser = '" + username + "'");
			while(rs.next()) {
				String tmpFilename = rs.getString("fileName");
				fileList.add(tmpFilename);
			}
				
		} catch (SQLException sqle) {
			System.out.println("sqle: " + sqle.getMessage());
		} 
		
		String[] fileArray = new String[fileList.size()];
		fileArray = fileList.toArray(fileArray);
		return fileArray;
	}

	public boolean hasAccess(String filename, String username, String owner) {
		
		if(username.equals(owner)) {
			return true;
		} else {
			String[] accessibleFiles = getAccessibleFiles(username, owner);
			for(int i = 0; i < accessibleFiles.length; i++) {
				if(accessibleFiles[i].equals(filename)) {
					return true;
				}
			}
		}
		return false;
	}

}
