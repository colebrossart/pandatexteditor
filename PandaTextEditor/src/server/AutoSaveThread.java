package server;

import java.util.Vector;

public class AutoSaveThread extends Thread{
	
	private Vector<PandaServerClientCommunicator> psccVector;
	
	public AutoSaveThread(Vector<PandaServerClientCommunicator> psccVector) {
		this.psccVector = psccVector;
	}
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(12000);			
			} catch (InterruptedException e) {
				break;
			}
			for(int i = 0; i < psccVector.size(); i++) {
				psccVector.get(i).saveOpenFiles();
			}
			
		}
	}
}
