DROP DATABASE IF EXISTS cbrossar;
CREATE DATABASE cbrossar;
USE cbrossar;

CREATE TABLE UserInfo (
	username VARCHAR(45) PRIMARY KEY NOT NULL,
    pword VARCHAR(45) NOT NULL
);

CREATE TABLE FileTable (
	fileID INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    fileName VARCHAR(45) NOT NULL,
    ownerName VARCHAR(45) NOT NULL,
    FOREIGN KEY fk1(ownername) REFERENCES UserInfo(username)
);

CREATE TABLE FileShare (
	fileID INT(11) NOT NULL,
    sharedUser VARCHAR(45) NOT NULL,
    FOREIGN KEY fk2(fileID) REFERENCES FileTable(fileID),
    FOREIGN KEY fk1(sharedUser) REFERENCES UserInfo(username)
    
);

